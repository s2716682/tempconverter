package nl.utwente.di.TempConverter;

public class FahrenheitConverter {

    public double getFahrenheit(String temp) {
        return (Integer.parseInt(temp) * 1.8) + 32;
    }
}
