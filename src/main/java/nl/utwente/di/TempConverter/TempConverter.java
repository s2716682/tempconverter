package nl.utwente.di.TempConverter;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class TempConverter extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private FahrenheitConverter converter;
	
    public void init() throws ServletException {
    	converter = new FahrenheitConverter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "TempConverter";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celcius Temp: " +
                   request.getParameter("Temp") + "\n" +
                "  <P>Fahrenheit: " +
                   Double.toString(converter.getFahrenheit(request.getParameter("Temp"))) +
                "</BODY></HTML>");
  }
  

}
